/** 
 * vue代码执行的入口（最先执行的）
 */
// 1. 导入vue的构造函数
import Vue from 'vue'
// 2. 导入根组件
// import App from './App.vue'
// 换根App
// import App from './views/scoped解决样式冲突/index.vue'
// 说明：导入目录下的组件叫index.vue，可以省略不写
// import App from './views/data必须是一个函数'
// import App from './views/父子通信'
// import App from './views/props作用'
// import App from './views/props校验'
// import App from './views/provide&inject'
// import App from './views/v-model原理'
// import App from './views/表单类组件封装'
// import App from './views/ref和$refs'
// import App from './views/$nextTick'
// import App from './views/自定义指令'
// import App from './views/v-loading指令的封装'
// import App from './views/组件插槽'
import App from './views/案例-商品列表'




// 导入要全局注册的组件
import Button from './components/button.vue'

// 注册全局组件=》语法：Vue.component('组件元素名', 组件对象)
Vue.component('Button', Button)

/**
 * 2. 全局注册指令
 * Vue.directive('指令名', {
  inserted (el) {
    // 可以对 el 标签，扩展额外功能
    el.focus()
  }
})
 */
Vue.directive('bord', {
  inserted (el, binding) {
    console.log('绑定到值：', binding)
    el.style.border = `2px solid ${binding.value}`
  },
  update (el, binding) {
    console.log('每次修改变量都执行：', binding.value)
    el.style.border = `2px solid ${binding.value}`

  }
})

Vue.directive('focus', {
  inserted (el) {
    // console.log("使用指令的元素dom:", el);
    el.focus();
  },
})


// 生产环境提示
Vue.config.productionTip = false

// 3. 创建vue实例
new Vue({
  // render函数：渲染根组件
  render: h => h(App),
}).$mount('#app')

