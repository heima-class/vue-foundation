import Vue from 'vue'
import App from './App.vue'
// 在入口导入的样式（全局=》作用于所有组件）
import './styles/base.css' // css 样式重置
import './styles/common.css' // 公共全局样式
import './assets/iconfont/iconfont.css' // 字体图标的样式

import './styles/index.css' // 页面样式

// 导入全局注册组件
import XtxShortCut from './components/XtxShortCut.vue'
import XtxHeaderNav from './components/XtxHeaderNav.vue'
Vue.component('XtxShortCut', XtxShortCut)
Vue.component('XtxHeaderNav', XtxHeaderNav)


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
