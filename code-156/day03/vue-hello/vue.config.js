const { defineConfig } = require('@vue/cli-service')
// webpack打包配置
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false
})
