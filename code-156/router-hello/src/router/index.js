import Vue from 'vue'
import VueRouter from "vue-router";

// 导入页面
// 一级页面
import Layout from '@/views/Layout.vue'
import ArticleDetail from '@/views/ArticleDetail.vue'
// 二级页面
import Article from '@/views/Article.vue'
import Collect from '@/views/Collect.vue'
import Like from '@/views/Like.vue'
import User from '@/views/User.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      // 重定向跳转第一个子页面：/article 文章列表
      redirect: '/article'
    },
    // 首页（一级）
    {
      // path: '/home',
      path: '/',
      // 现在父路由地址是？/
      component: Layout,
      // 1. ==首页下==二级路由配置 =》children:[]
      /**
       * 二级路由使用场景：
       * 系统开发需要使用菜单切换页面
       * 问题：菜单能不能放到App.vue中？不建议
       * 技术栈：使用嵌套路由（父子路由）
       * 步骤：
       * 1. 菜单和子路由挂载点放到父路由（一级路由）
       * 2. 子路由配置到父路由下边（children）
       */

      children:[
        // 说明：二级路由path不用加/,只用配置儿子的path
        // 技巧：自路由的router-view就放到父路由对应组件下边
        //children中的配置项 跟一级路由中的配置项一模一样 

        // == 掌握（完整路径）：子路由地址 = 父路由地址+子路由地址  ==
        {path:'/article',component:Article},
        {path:'/collect',component:Collect},
        {path:'/like',component:Like},
        {path:'/user',component:User},

      ]
    },
    // 新闻详情页（一级）
    {
      path: '/detail',
      component: ArticleDetail
    }
  ]
})

export default router