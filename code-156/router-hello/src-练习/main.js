/**
 * vue程序执行入口
 */
import Vue from 'vue'
import App from './App.vue'

// 导入路由实例
import router from '@/router'


Vue.config.productionTip = false



// 核心：创建实例，挂载渲染App.vue(一个)组件
new Vue({
  render: h => h(App),
  // 4. 挂载路由实例到vue组件实例上
  // 作用：将来组件中可用通过：this.$router访问路由实例对象 | this.$route访问路由参数对象
  router: router
}).$mount('#app')
