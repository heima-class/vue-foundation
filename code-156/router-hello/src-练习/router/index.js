/**
 * 路由实例化
 */
import Vue from 'vue'
// 1. 导入路由的构造函数
import VueRouter from 'vue-router'

// 导入路由的页面
// 说明：@/指向src目录
import Home from '@/views/home'
import About from '../views/about'
import Search from '@/views/search/search.vue'
import Result from '@/views/search/result.vue'
import NotFound from '@/views/404'



// 2. 注册路由
Vue.use(VueRouter)

// 3. 创建路由的实例（对象）
const router = new VueRouter({
  // 配置路由模式: 1. hash模式（路径带#）默认，兼容性好 2. history模式（优雅）
  // mode: 'hash',
  mode: 'history',
  // 将来用来配置：path(地址)和page（页面/.vue组件）映射关系
  // 思考？将来浏览器地址栏输入path,对应组件显示到页面中，是不是要指定一个路由的出口，渲染页面组件？
  // 回答：在App.vue中放置一个叫 <router-view></router-view>
  //  路由映射关系
  // 重点掌握：在routes数组中配置路由
  routes: [
    // 地址栏什么也没有输入，就会跳转redirect指定的path地址
    { path: '/', redirect: '/home' },
    // 第一个页面路由：首页
    {
      path: '/home', // 地址=》名字自己起，以/开头
      component: Home // 地址输完显示的页面组件
    },
    // 第二个页面路由：关于我们
    {
      path: '/about', // 地址
      component: About // 地址输完显示的页面组件
    },
    // 搜索相关页面
    {
      path: '/search', // 地址
      component: Search // 地址输完显示的页面组件
    },
    {
      name: 'result', // 给路由起名字，可以使用名字跳页面
      // 动态路由（带参数，参数名子的提前定好，以:号开头）=> path地址/:参数名1/:参数名2...
      // 注意：1. 动态路由定义好参数名后，跳转页面必须带上参数值
      // 如果不想参数值： path地址/:参数名1?=》加个?号可选符
      path: '/result/:keyword?', // 地址
      component: Result // 地址输完显示的页面组件
    },
    // 场景：用户不小心输错了或者网页更新有老的页面地址跳转
    // 说明：* (通配符)任意没有匹配到的path地址，就会渲染*地址的页面
    { path: '*', component: NotFound } //最后一个
  ]
})

// 导出路由实例
export default router