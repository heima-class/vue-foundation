const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 保留eslint检查，但不会影响代码执行
  lintOnSave: true,
  // 开发服务器弹层警告
  devServer: {
    client: {
      overlay: false
    }
  }
})
