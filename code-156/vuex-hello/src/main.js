import Vue from 'vue'
import App from './App.vue'
// 1. 导入vuex的实例（仓库）
import store from './store'

Vue.config.productionTip = false

new Vue({
  // 2. 把vuex实例挂载到vue实例=》将来组件中就可以通过this.$store获取到vuex实例
  store,
  render: h => h(App)
}).$mount('#app')
