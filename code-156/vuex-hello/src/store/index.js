/**
 * 实例化vuex
 */
import Vue from 'vue'
// 1. 导入Vuex
import Vuex from 'vuex'
// 2. 注册Vuex
Vue.use(Vuex)

// 3. 实例化Vuex创建store（仓库）=》存储全局状态数据
export default new Vuex.Store({
  strict: true, // 开启严格模式
  // 1. 定义全局共享数据
  state: {
  // state 状态, 即数据, 类似于vue组件中的data,
  // 语法：key(变量名):value(变量默认值)
  // 区别：
  // 1.data 是组件自己的数据,
  // 2.state 中的数据整个vue项目的组件都能访问到
    count: 1,
    obj: {
      name: '喵爷',
      age: 18
    }
  },
  getters: {
  },
  // 2. 定义修改数据的函数（预习）
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
